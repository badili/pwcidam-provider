# PWCIdAM OAuth2 Provider for Laravel Socialite

[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/g/SocialiteProviders/PWCIdAM.svg?style=flat-square)](https://scrutinizer-ci.com/g/SocialiteProviders/PWCIdAM/?branch=master)
[![Latest Stable Version](https://img.shields.io/packagist/v/socialiteproviders/pwcidam.svg?style=flat-square)](https://packagist.org/packages/socialiteproviders/pwcidam)
[![Total Downloads](https://img.shields.io/packagist/dt/socialiteproviders/pwcidam.svg?style=flat-square)](https://packagist.org/packages/socialiteproviders/pwcidam)
[![Latest Unstable Version](https://img.shields.io/packagist/vpre/socialiteproviders/pwcidam.svg?style=flat-square)](https://packagist.org/packages/socialiteproviders/pwcidam)
[![License](https://img.shields.io/packagist/l/socialiteproviders/pwcidam.svg?style=flat-square)](https://packagist.org/packages/socialiteproviders/pwcidam)

## Documentation

Full documentation for using this provider can be found at [PWCIdAM Documentation](http://socialiteproviders.github.io/providers/pwcidam/)

## INSTALLATION

## COMPOSER

// This assumes that you have composer installed globally

```composer require socialiteproviders/pwcidam```

## SERVICE PROVIDER

Remove ```Laravel\Socialite\SocialiteServiceProvider from your providers[] array in config\app.php``` if you have added it already.

Add ```\SocialiteProviders\Manager\ServiceProvider::class to your providers[] array in config\app.php```.

#### For example:

```
'providers' => [
    // a whole bunch of providers
    // remove 'Laravel\Socialite\SocialiteServiceProvider',
    \SocialiteProviders\Manager\ServiceProvider::class, // add
];
```
Note: If you would like to use the Socialite Facade, you need to install it.

## ADD THE EVENT AND LISTENERS

Add SocialiteProviders\Manager\SocialiteWasCalled event to your listen[] array in ``` <app_name>/Providers/EventServiceProvider```.

Add your listeners (i.e. the ones from the providers) to the  ```SocialiteProviders\Manager\SocialiteWasCalled[]```that you just created.

The listener that you add for this provider is  ```'SocialiteProviders\Pwcidam\PwcidamExtendSocialite@handle'```,.

Note: You do not need to add anything for the built-in socialite providers unless you override them with your own providers.

#### For example:

````
/**
 * The event handler mappings for the application.
 *
 * @var array
 */
protected $listen = [
    \SocialiteProviders\Manager\SocialiteWasCalled::class => [
        // add your listeners (aka providers) here
        'SocialiteProviders\Pwcidam\PwcidamExtendSocialite@handle',
    ],
];
```

## REFERENCE

Laravel docs about events
Laracasts video on events in Laravel 5

## ENVIRONMENT VARIABLES

If you add environment values to your .env as exactly shown below, you do not need to add an entry to the services array.

The values below are provided as a convenience in the case that a developer is not able to use the .env method

```
'pwcidam' => [
    'client_id' => env('APP_KEY_ID'),
    'client_secret' => env('APP_SECRET_KEY'),
    'redirect' => env('YOUR_REDIRECT_URI'),  
], 
```

## REFERENCE

Laravel docs on configuration

## USAGE

You should now be able to use it like you would regularly use Socialite (assuming you have the facade installed):
```return Socialite::with('Pwcidam')->redirect();```

## LUMEN SUPPORT

You can use Socialite providers with Lumen. Just make sure that you have facade support turned on and that you follow the setup directions properly.

Note: If you are using this with Lumen, all providers will automatically be stateless since Lumen does not keep track of state.

Also, configs cannot be parsed from the services[] in Lumen. You can only set the values in the .env file as shown exactly in this document. If needed, you can also override a config (shown below).

## STATELESS

You can set whether or not you want to use the provider as stateless. Remember that the OAuth provider (Twitter, Tumblr, etc) must support whatever option you choose.
Note: If you are using this with Lumen, all providers will automatically be stateless since Lumen does not keep track of state.

// to turn off stateless
```return Socialite::with('pwcidam')->stateless(false)->redirect();```

// to use stateless
```return Socialite::with('pwcidam')->stateless()->redirect();```