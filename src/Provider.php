<?php

namespace SocialiteProviders\PWCIdAM;

use Laravel\Socialite\Two\ProviderInterface;
use SocialiteProviders\Manager\OAuth2\AbstractProvider;
use SocialiteProviders\Manager\OAuth2\User;

use Illuminate\Support\Arr;
use GuzzleHttp\ClientInterface;

class Provider extends AbstractProvider implements ProviderInterface
{
    /**
     * Unique Provider Identifier.
     */
    const IDENTIFIER = 'PWCIDAM';

    /**
     * {@inheritdoc}
     */
    // dev server
    // protected $baseUrl = 'https://fedsvc-stage.pwc.com';
    // production
    protected $baseUrl = 'https://fedsvc.pwc.com';
    protected $fields = ['name', 'emailaddress', 'immutableid'];
    protected $scopes = ['openid'];

    /**
     * {@inheritdoc}
     */
    protected $popup = false;

    /**
     * Re-request a declined permission.
     *
     * @var bool
     */
    protected $reRequest = false;

    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase($this->baseUrl.'/ofis', $state);
    }
    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return $this->baseUrl.'/ofisid/api/access';
    }

    public function getAccessTokenResponse($code)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';
        $fields_to_send = $this->getTokenFields($code);
        $fields_to_send["grant_type"]="authorization_code";

        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            $postKey => $fields_to_send,
        ]);

        $data = [];
        parse_str($response->getBody(), $data);

        return json_decode($response->getBody(), true);
    }


    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        $meUrl = $this->baseUrl.'/ofisids/api/userprofile';
        $response = $this->getHttpClient()->get($meUrl, [
            'headers' => [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer '.$token,
            ],
        ]);
        
        return json_decode($response->getBody(), true);
    }
    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map(
            [
            'sub' => $user['sub'], 
            'name' => isset($user['given_name']) ? $user['given_name'] : null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getCodeFields($state = null)
    {
        $fields = parent::getCodeFields($state);

        if ($this->popup) {
            $fields['display'] = 'popup';
        }

        if ($this->reRequest) {
            $fields['auth_type'] = 'rerequest';
        }

        return $fields;
    }

    /**
     * Set the user fields to request from Pwc .
     *
     * @param  array  $fields
     * @return $this
     */
    public function fields(array $fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Set the dialog to be displayed as a popup.
     *
     * @return $this
     */
    public function asPopup()
    {
        $this->popup = true;

        return $this;
    }

    /**
     * Re-request permissions which were previously declined.
     *
     * @return $this
     */
    public function reRequest()
    {
        $this->reRequest = true;
    }
}