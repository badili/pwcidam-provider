<?php

namespace SocialiteProviders\PWCIdAM;

use SocialiteProviders\Manager\SocialiteWasCalled;

class PwcidamExtendSocialite
{
    /**
     * Execute the provider.
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('pwcidam', __NAMESPACE__.'\Provider');
    }
}
